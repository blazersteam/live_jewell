# <!=Copyright (C) 2018 Blossom Themes
# This file is distributed under the GNU General Public License v2 or later.=!>
msgid ""
msgstr ""
"Project-Id-Version: Blossom Consulting 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/theme/style\n"
"POT-Creation-Date: 2018-07-04 08:59:33+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2018-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n1.0.0\n"

#: functions.php:148
msgid " Blossom Consulting"
msgstr ""

#: functions.php:148
msgid " by Blossom Themes."
msgstr ""

#: functions.php:149
msgid " Powered by %s"
msgstr ""

#: functions.php:149
msgid "https://wordpress.org/"
msgstr ""

#: inc/customizer.php:32
msgid "Site Title Font Size"
msgstr ""

#: inc/customizer.php:33
msgid "Change the font size of your site title."
msgstr ""

#: inc/customizer.php:50
msgid "Banner Options"
msgstr ""

#: inc/customizer.php:51
msgid "Choose banner as static image/video or as a slider."
msgstr ""

#: inc/customizer.php:54
msgid "Disable Banner Section"
msgstr ""

#: inc/customizer.php:55
msgid "Static/Video CTA Banner"
msgstr ""

#: inc/customizer.php:56
msgid "Static/Video Newsletter Banner"
msgstr ""

#: inc/customizer.php:57
msgid "Banner as Slider"
msgstr ""

#: inc/customizer.php:68 inc/customizer.php:360 sections/banner.php:13
msgid "The Secrets to Successful Team Leadership"
msgstr ""

#: inc/customizer.php:77
msgid "Title"
msgstr ""

#: inc/customizer.php:93 inc/customizer.php:367 sections/banner.php:14
msgid ""
"Sally is a solution focused therapist offering brief and often single "
"session therapy. She has built a reputation for engaging workshops on "
"Solution Focused Brief Therapy and Single Session Therapy, and have "
"recently started to put her courses online."
msgstr ""

#: inc/customizer.php:102
msgid "Sub Title"
msgstr ""

#: inc/customizer.php:118 inc/customizer.php:374 sections/banner.php:15
msgid "Discover More"
msgstr ""

#: inc/customizer.php:127
msgid "Banner Label"
msgstr ""

#: inc/customizer.php:151
msgid "Banner Link"
msgstr ""

#: inc/customizer.php:162
msgid "Shop Section"
msgstr ""

#: inc/customizer.php:183
msgid "Enable Shop Section"
msgstr ""

#: inc/customizer.php:184
msgid "Enable to show shop section."
msgstr ""

#: inc/customizer.php:194 inc/customizer.php:381 sections/shop.php:8
msgid "Shop"
msgstr ""

#: inc/customizer.php:204
msgid "Section Title"
msgstr ""

#: inc/customizer.php:219 inc/customizer.php:388
msgid ""
"Show your products here. You can modify this section from Appearance > "
"Customize > Front Page Settings > Shop Section."
msgstr ""

#: inc/customizer.php:229
msgid "Section Content"
msgstr ""

#: inc/customizer.php:254
msgid "Product One"
msgstr ""

#: inc/customizer.php:275
msgid "Product Two"
msgstr ""

#: inc/customizer.php:296
msgid "Product Three"
msgstr ""

#: inc/customizer.php:317
msgid "Product Four"
msgstr ""

#: inc/customizer.php:326
msgid "Demo & Documentation"
msgstr ""

#: inc/customizer.php:339
msgid "Demo Link: %1$sClick here.%2$s"
msgstr ""

#: inc/customizer.php:341
msgid "Documentation Link: %1$sClick here.%2$s"
msgstr ""

#: sections/shop.php:9
msgid ""
"Show your latest blog posts here. You can modify this section from "
"Appearance > Customize > Front Page Settings > Shop Section."
msgstr ""

#: sections/shop.php:45
msgid "Sold Out"
msgstr ""

#. Theme Name of the plugin/theme
msgid "Blossom Consulting"
msgstr ""

#. Theme URI of the plugin/theme
msgid "https://blossomthemes.com/downloads/blossom-consulting-free-wordpress-theme/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Blossom Consulting is free child theme of Blossom Consulting WordPress "
"theme. Blossom Consulting is a perfect choice to create websites for "
"consultants and consulting companies, coaches (life coach, health coach), "
"speakers, business and creative agencies, content marketers, affiliate "
"bloggers and psychologists. The theme is also multi-purpose. So you can "
"also use it to build small business (restaurants/cafe, travel, education, "
"hotel, construction, events, wedding planners, fitness, affiliate, fashion, "
"lawyer, sport/medical shops, spa/temas, political), portfolio, church, "
"online agencies and firms, charity, ecommerce (WooCommerce), and "
"freelancers websites. It is responsive, Schema.org compatible, SEO "
"friendly, RTL compatible, speed optimized, and translation ready. Check "
"theme details at "
"https://blossomthemes.com/downloads/blossom-consulting-free-wordpress-theme/"
", demo at https://demo.blossomthemes.com/blossom-consulting/, read the "
"documentation at "
"https://blossomthemes.com/blossom-consulting-documentation/, and get "
"support at https://blossomthemes.com/support-ticket/."
msgstr ""

#. Author of the plugin/theme
msgid "Blossom Themes"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://blossomthemes.com/"
msgstr ""