<?php
/**
 * Theme functions and definitions
 *
 * @package Blossom_Consulting
 */
 
/**
 * Enqueue parent style 
*/	 
function blossom_consulting_enqueue_styles() {
    
    if( blossom_coach_is_woocommerce_activated() ){
        $dependencies = array( 'blossom-coach-woocommerce', 'font-awesome', 'owl-carousel', 'animate', 'blossom-coach-google-fonts' );    
    }else{
        $dependencies = array( 'font-awesome', 'owl-carousel', 'animate', 'blossom-coach-google-fonts' );
    }
        
    wp_enqueue_style( 'blossomm-consulting-parent-style', get_template_directory_uri() . '/style.css', $dependencies );

} 
add_action( 'wp_enqueue_scripts', 'blossom_consulting_enqueue_styles' );

/**
 * Sets up theme defaults and registers support for various WordPress features.
*/
function blossom_consulting_setup(){
    /*
     * Make chile theme available for translation.
     * Translations can be filed in the /languages/ directory.
     */
    load_child_theme_textdomain( 'blossom-consulting', get_stylesheet_directory() . '/languages' );

    //add image size
    add_image_size( 'blossom-coach-shop', 540, 690, true );
}
add_action( 'after_setup_theme', 'blossom_consulting_setup' );

/**
 * Remove action from parent
*/
function blossom_consulting_remove_action(){
    remove_action( 'wp_head', 'blossom_coach_dynamic_css', 99 );
    remove_action( 'customize_register', 'blossom_coach_customizer_theme_info' );
}
add_action( 'init', 'blossom_consulting_remove_action' );

/**
 * Dynamic Styles
*/
function blossom_consulting_dynamic_css(){
    
    $primary_font    = get_theme_mod( 'primary_font', 'Nunito Sans' );
    $primary_fonts   = blossom_coach_get_fonts( $primary_font, 'regular' );
    $secondary_font  = get_theme_mod( 'secondary_font', 'Nunito' );
    $secondary_fonts = blossom_coach_get_fonts( $secondary_font, 'regular' );
    
    $site_title_font      = get_theme_mod( 'site_title_font', array( 'font-family'=>'Nunito', 'variant'=>'700' ) );
    $site_title_fonts     = blossom_coach_get_fonts( $site_title_font['font-family'], $site_title_font['variant'] );
    $site_title_font_size = get_theme_mod( 'site_title_font_size', 25 );
    
    echo "<style type='text/css' media='all'>"; ?>
    
    /*Typography*/
    body,
    button,
    input,
    select,
    optgroup,
    textarea, section[class*="-section"] .widget_blossom_client_logo_widget .widget-title, .blog-section article .entry-meta, 
    .btn-link, .widget.widget_blossomthemes_stat_counter_widget .widget-title, .single .entry-meta, 
    .portfolio-text-holder .portfolio-img-title {
        font-family : <?php echo esc_html( $primary_fonts['font'] ); ?>;
    }
    
    .site-title, 
    .site-title-wrap .site-title{
        font-size   : <?php echo absint( $site_title_font_size ); ?>px;
        font-family : <?php echo esc_html( $site_title_fonts['font'] ); ?>;
        font-weight : <?php echo esc_html( $site_title_fonts['weight'] ); ?>;
        font-style  : <?php echo esc_html( $site_title_fonts['style'] ); ?>;
    }
    
    /*Typography*/
    h1, h2, h3, h4, h5, h6, 
    section[class*="-section"] .widget .widget-title,
section[class*="-section"] .widget_blossomtheme_featured_page_widget .section-subtitle,
.section-title, .comment-body b.fn, .comment-body .reply .comment-reply-link, .single .navigation .nav-links, 
.site-header .header-search label.screen-reader-text, .btn-readmore, .btn-readmore:visited, .bttk-testimonial-holder .name, 
.pricing-block .price, .entry-meta, #primary .widget_blossomtheme_featured_page_widget .section-subtitle, 
.widget_blossomthemes_stat_counter_widget .hs-counter, .widget_bttk_description_widget .bttk-team-holder .name, 
.bttk-team-inner-holder-modal .name, .page-header .subtitle, .dropcap, .error-404 .error-num, .error-404 a.bttn, 
.related-portfolio-title {
        font-family: <?php echo esc_html( $secondary_fonts['font'] ); ?>;
    }
    <?php if( blossom_coach_is_woocommerce_activated() ) { ?>
        .woocommerce div.product .product_title,
        .woocommerce div.product .woocommerce-tabs .panel h2{
            font-family: <?php echo esc_html( $secondary_fonts['font'] ); ?>;
        }    
    <?php } ?>
           
    <?php echo "</style>";
}
add_action( 'wp_head', 'blossom_consulting_dynamic_css', 99 );

/**
 * Returns Home Sections 
*/
function blossom_coach_get_home_sections(){
    $ed_banner = get_theme_mod( 'ed_banner_section', 'slider_banner' );
    $sections = array( 
        'client'      => array( 'sidebar' => 'client' ),
        'about'       => array( 'sidebar' => 'about' ),
        'cta'         => array( 'sidebar' => 'cta' ),
        'testimonial' => array( 'sidebar' => 'testimonial' ),
        'service'     => array( 'sidebar' => 'service' ),
        'blog'        => array( 'section' => 'blog' ),
        'simple-cta'  => array( 'sidebar' => 'simple-cta' ),
        'shop'        => array( 'section' => 'shop' ),
        'contact'     => array( 'sidebar' => 'contact' ), 
    );
    
    $enabled_section = array();
    
    if( $ed_banner == 'static_nl_banner' || $ed_banner == 'slider_banner' || $ed_banner == 'static_banner' ) array_push( $enabled_section, 'banner' );
    
    foreach( $sections as $k => $v ){
        if( array_key_exists( 'sidebar', $v ) ){
            if( is_active_sidebar( $v['sidebar'] ) ) array_push( $enabled_section, $v['sidebar'] );
        }else{
            if( get_theme_mod( 'ed_' . $v['section'] . '_section', true ) ) array_push( $enabled_section, $v['section'] );
        }
    }  
    
    return apply_filters( 'blossom_coach_home_sections', $enabled_section );
}

/**
 * Footer Bottom
*/
function blossom_coach_footer_bottom(){ ?>
    <div class="bottom-footer">
        <div class="wrapper">
            <div class="copyright">            
            <?php
                blossom_coach_get_footer_copyright();
                echo '<a href="' . esc_url( 'https://blossomthemes.com/downloads/blossom-consulting/' ) .'" rel="author" target="_blank">' . esc_html__( ' Blossom Consulting', 'blossom-consulting' ) . '</a>' . esc_html__( ' by Blossom Themes.', 'blossom-consulting' );                
                printf( esc_html__( ' Powered by %s', 'blossom-consulting' ), '<a href="'. esc_url( __( 'https://wordpress.org/', 'blossom-consulting' ) ) .'" target="_blank">WordPress</a>.' );
                if ( function_exists( 'the_privacy_policy_link' ) ) {
                    the_privacy_policy_link();
                }
            ?>               
            </div>
        </div><!-- .wrapper -->
    </div><!-- .bottom-footer -->
    <?php
}

/**
 * Customizer Controls
*/
require get_stylesheet_directory() . '/inc/customizer.php';