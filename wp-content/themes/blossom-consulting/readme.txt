=== Blossom Consulting ===

Contributors: Blossom Themes (https://blossomthemes.com/)
Requires at least: 4.7
Tested up to: 4.9.6
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: blog, one-column, two-columns, right-sidebar, left-sidebar, footer-widgets, custom-background, custom-header, custom-menu, custom-logo, featured-images, threaded-comments, full-width-template, rtl-language-support, translation-ready, theme-options, e-commerce

== Description ==
Blossom Consulting is free child theme of Blossom Consulting WordPress theme. Blossom Consulting is a perfect choice to create websites for consultants and consulting companies, coaches (life coach, health coach), speakers, business and creative agencies, content marketers, affiliate bloggers and psychologists. The theme is also multi-purpose. So you can also use it to build small business (restaurants/cafe, travel, education, hotel, construction, events, wedding planners, fitness, affiliate, fashion, lawyer, sport/medical shops, spa/temas, political), portfolio, church, online agencies and firms, charity, ecommerce (WooCommerce), and freelancers websites. It is responsive, Schema.org compatible, SEO friendly, RTL compatible, speed optimized, and translation ready. Check theme details at https://blossomthemes.com/downloads/blossom-consulting-free-wordpress-theme/, demo at https://demo.blossomthemes.com/blossom-consulting/, read the documentation at https://blossomthemes.com/blossom-consulting-documentation/, and get support at https://blossomthemes.com/support-ticket/.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Copyright ==

Blossom Consulting WordPress Theme, Copyright 2017 blossomthemes.com
Blossom Consulting is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Images used in screenshots.
Source:
https://pixabay.com/en/workplace-team-business-meeting-1245776/
License: CC0, https://pixabay.com/en/service/terms/#usage

All other images including icon image and images used in customizer settings are self created and are under the GPL.

== Changelog ==    
    1.0.0
    * Initial release